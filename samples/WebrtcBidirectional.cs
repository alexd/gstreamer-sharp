using System;
using GLib;
using Gst;
using Gst.WebRTC;

namespace GstreamerSharp
{
	class Playback
	{
		static Pipeline Pipeline;
		static Element Webrtc1;
		static Element Webrtc2;

		public static void Main (string[] args)
		{
			// Initialize Gstreamer
			Gst.Application.Init (ref args);

			// Build the pipeline
			Pipeline = (Pipeline) Parse.Launch (
				"videotestsrc pattern=smpte ! queue ! vp8enc ! rtpvp8pay ! queue ! " +
				"application/x-rtp,media=video,payload=96,encoding-name=VP8 ! webrtcbin name=webrtc1 " +
				"videotestsrc pattern=ball  ! queue ! vp8enc ! rtpvp8pay ! queue ! " +
				"application/x-rtp,media=video,payload=96,encoding-name=VP8 ! webrtcbin name=webrtc2"
			);

			// Wire the webrtcbins
			Webrtc1 = Pipeline.GetByName ("webrtc1");
			Webrtc2 = Pipeline.GetByName ("webrtc2");

			Webrtc1.Connect ("on-negotiation-needed", OnNegotiationNeeded);
			Webrtc1.Connect ("on-ice-candidate", OnIceCandidate);
			Webrtc2.Connect ("on-ice-candidate", OnIceCandidate);
			Webrtc1.Connect ("pad-added", PadAdded);
			Webrtc2.Connect ("pad-added", PadAdded);

			// Start playing
			Pipeline.SetState (State.Playing);

			// There won't be an EOS, wait for the user to press enter
			Console.ReadLine ();

			// Free resources
			Pipeline.SetState (State.Null);
		}

		static void OnNegotiationNeeded (object o, SignalArgs args)
		{
			var promise = new Promise (OnOfferReceived, Webrtc1.Handle, null);
			Webrtc1.Emit ("create-offer", /* options */ null, promise);
		}

		static void OnOfferReceived (Promise promise)
		{
			if (promise.Wait () != PromiseResult.Replied)
			{
				Console.WriteLine ("Failed to create SDP offer.");
				return;
			}

			var reply = promise.RetrieveReply ();
			var offer = reply.GetValue ("offer");
			var sessionDescription = (WebRTCSessionDescription) offer.Val;

			Console.WriteLine ("Created SDP offer:");
			Console.WriteLine (sessionDescription.Sdp.AsText ());

			Webrtc1.Emit ("set-local-description", sessionDescription, /* promise */ null);
			Webrtc2.Emit ("set-remote-description", sessionDescription, /* promise */ null);

			promise = new Promise (OnAnswerReceived, Webrtc2.Handle, null);
			Webrtc2.Emit ("create-answer", /* options */ null, promise);
		}

		static void OnAnswerReceived (Promise promise)
		{
			if (promise.Wait () != PromiseResult.Replied)
			{
				Console.WriteLine ("Failed to create SDP answer.");
				return;
			}

			var reply = promise.RetrieveReply ();
			var answer = reply.GetValue ("answer");
			var sessionDescription = (WebRTCSessionDescription) answer.Val;

			Console.WriteLine ("Created SDP answer:");
			Console.WriteLine (sessionDescription.Sdp.AsText ());

			Webrtc1.Emit ("set-remote-description", sessionDescription, /* promise */ null);
			Webrtc2.Emit ("set-local-description", sessionDescription, /* promise */ null);
			
			Console.WriteLine ("Two video windows should open momentarily ...");
			Console.WriteLine ("Press enter to exit ...");
		}

		static void OnIceCandidate (object o, SignalArgs args)
		{
			var mlineIndex = (uint) args.Args [0];
			var candidate = (string) args.Args [1];

			var other = ReferenceEquals (o, Webrtc1) ? Webrtc2 : Webrtc1;
			other.Emit ("add-ice-candidate", mlineIndex, candidate);
		}

		static void PadAdded (object o, SignalArgs args)
		{
			var srcpad = (Pad) args.Args [0];
			if (srcpad.Direction != PadDirection.Src)
				return;

			var sink = Parse.BinFromDescription ("rtpvp8depay ! vp8dec ! videoconvert ! queue ! xvimagesink", true);

			Pipeline.Add (sink);
			sink.SyncStateWithParent ();

			var sinkpad = (Pad) sink.Sinkpads[0];
			srcpad.Link (sinkpad);
		}
	}
}